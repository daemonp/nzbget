FROM alpine:latest

RUN apk --update add bash curl wget zip p7zip ffmpeg python3 git par2cmdline sudo

RUN DOWNLOAD_URL=`curl -Ls  http://nzbget.net/download | grep -o '[^"'"'"']*run' | grep 'http.*linux.run' | tail -1` && \
    echo $DOWNLOAD_URL && \
    curl -L $DOWNLOAD_URL -o /nzbget.run && \
  sh /nzbget.run && \
  rm -f /nzbget.run && \
  adduser -h /data -s /bin/sh -G video -u 1000 video -D && \
  mkdir -p /etc/sudoers.d && \
  echo "video ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/video && \
  chmod 0440 /etc/sudoers.d/video
  
ADD ./start.sh /nzbget/start.sh

USER video

EXPOSE 6789

ENTRYPOINT ["/nzbget/start.sh"]

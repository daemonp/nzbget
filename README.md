# nzbget container for media server

### docker-compose.yml
```
  nzbget:
    image: registry.gitlab.com/daemonp/nzbget:latest
    ports:
      - "6789:6789"
    volumes:
      - "/share/data/:/data"
      - "/etc/localtime:/etc/localtime:ro"
    restart: always
```

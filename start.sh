#!/bin/bash

set -e

[ ! -f /data/config/nzbget.conf ] && cp /nzbget/nzbget.conf /data/config/nzbget.conf

/nzbget/nzbget --configfile /data/config/nzbget.conf -s -o OutputMode=log -P
sleep 5
/nzbget/nzbget --configfile /data/config/nzbget.conf --unpause

